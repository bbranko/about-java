package kds.spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

//Configuration annotation can also be used in conjunction with @PropertySource for easy parsing and reading of configuration files.
@Configuration
//PropertySource is a mechanism to extend Spring environment data by the contents of files listed in it.
@PropertySource("classpath:application.properties")
//Spring-boot utilizes this mechanism to define a whole cascade of default source locations which allows for easy property
// externalization by convention and easy overriding when needed. Furthermore, in app, values can be easily mapped
// by name matching with use of @ConfigurationProperties annotation. More info:
// https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.external-config
public class PropertiesConfig {

  private final Environment environment;

  //and this here in annotation is SPeL!
  // Things to be evaluated are put within #{}, variables can be accessed with ${}, and syntax is very groovy-like
  //more info: https://docs.spring.io/spring-framework/reference/core/expressions.html
  @Value("${property.mapped.by.annotation}")
  private String property;

  //We can define default value if some property is missing like this:
  @Value("${missing.property:value if property is missing}")
  private String missingProperty;

  public PropertiesConfig(Environment environment) {
    this.environment = environment;
  }

  public String getMyCustomPropertyFromEnvironment() {
    return environment.getProperty("this.is.a.custom.property");
  }

  public String getMyCustomPropertyViaAnnotation() {
    return property;
  }

  public String getMyMissingProperty() {
    return missingProperty;
  }
}
