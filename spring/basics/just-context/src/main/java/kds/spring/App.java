package kds.spring;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import kds.spring.config.AppConfig;
import kds.spring.config.PropertiesConfig;
import kds.spring.service.LibraryService;

/**
 * We will make this class our entry point.
 * While usually start class also contains basic configuration for the spring framework, most importantly,
 * from where to start Component scan, but also which features of the framework should be activated,
 * this does not have to be the case! For that purpose, we will define a separate configuration and just load it in here.
 */
public class App {

  public static void main(String[] args) {
    //this is not part of the demo per se, but it does demonstrate that spring managed beans get wrapped in proxies.
    //uncomment next line to generate CGLIB proxies code at the specified path:
    //SpringCglibUtils.initGeneratedClassHandler("build/generated/cglib");
    //--


    //To start, we need to create Spring ApplicationContext. Within context creation, Spring DI gets setup,
    // as well as all other configured features and modules. ApplicationContext is the core of any spring application.

    //ApplicationContext is the base, but ConfigurableApplicationContext provides a bigger more feature complete API.

    //Furthermore, there are various contexts that can be set up depending on the type of processing we need (ie XML or Annotation)
    // or depending on the type of application we are running (ie WebAppContext when running spring-mvc app).

    //Finally, while we need to create context to start of the spring app, it is an antipattern to interact with it directly.
    // This is because it creates a coupling between our code and Spring API specifically, as opposed to following a specification
    // which a different framework or tool can make use of as a drop-in replacement for Spring should we need it.
    // Instead, we should have configurations and overrides in place and let Spring handle its internal state(context) by itself.

    //To set context up, we need to pass in our main Configuration, which we put in different class than this one.
    ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

    System.out.println("\n--basic usage demo--\n");
    //Now we can interact with context to get (now fully configured and linked) entrypoint of our application
    //note: we could request an Interface - and Spring will provide concrete implementation of it or throw an appropriate error!
    LibraryService springBean = context.getBean(LibraryService.class);
    //...and do some work with it.
    springBean.printoutBooks();


    System.out.println("\n--reading properties demo--\n");
    PropertiesConfig myCustomPropertyProvider = context.getBean(PropertiesConfig.class);
    String property1 = myCustomPropertyProvider.getMyCustomPropertyFromEnvironment();
    String property2 = myCustomPropertyProvider.getMyCustomPropertyViaAnnotation();
    String property3 = myCustomPropertyProvider.getMyMissingProperty();
    System.out.println("Property got from defined property source via environment: " + property1);
    System.out.println("Property got from defined property source via annotation: " + property2);
    System.out.println("Default value for missing property: " + property3);

    //Note: usually we would use Spring for implementing WebApp or some periodic task. In both cases,
    // Spring would internally set up these services without us having to interact with ApplicationContext at all.
    // Ie in case of WebApp Spring would set up and run a Server for us (tomcat by default),
    // and in case of a periodic task it would set up and run some form of Scheduled task execution.

    //it would happen anyway since our app ends here, but this is more explicit
    context.close();

  }


}
