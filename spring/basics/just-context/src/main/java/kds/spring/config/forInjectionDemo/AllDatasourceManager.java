package kds.spring.config.forInjectionDemo;

import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class AllDatasourceManager {

  public AllDatasourceManager(Collection<Datasource> myDatasources) {
    //Storing data is obsolete since we are only using this "Manager" classes for injection wiring demo:
    System.out.println("--\nAllDatasourceManager: ");
    myDatasources.forEach(datasource -> System.out.println(datasource.getData()));
    System.out.println("--");
  }

}
