package kds.spring.config.forInjectionDemo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class SpecificDatasourceManager {

  public SpecificDatasourceManager(@Qualifier("SpecificDatasource") Datasource myDatasource) {
    //Storing data is obsolete since we are only using this "Manager" classes for injection wiring demo:
    System.out.println("SpecificDatasourceManager: " + myDatasource.getData());
  }

}
