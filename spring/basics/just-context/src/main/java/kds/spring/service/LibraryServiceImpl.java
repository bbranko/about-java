package kds.spring.service;

import org.springframework.stereotype.Service;

import kds.spring.dao.BookDao;
import kds.spring.model.Book;

//Service is a subclass of Component, same as Configuration, Controller or Repository and as such candidate for DI.
//Purpose of all this different names is to differentiate class purpose since they can be managed and acted upon in different ways.
@Service
public class LibraryServiceImpl implements LibraryService {

  private final BookDao bookDao;

  //This will automatically do a constructor-based DI, and it will fail during app startup if dependencies are not satisfied.
  //Alternative is field injection, but it should only be done for optional dependencies.
  public LibraryServiceImpl(BookDao bookDao) {
    this.bookDao = bookDao;
  }

  @Override
  public void printoutBooks() {
    for (Book book : bookDao.findAll()) {
      System.out.println(book);
    }
  }
}
