package kds.spring.model;

/**
 * Not all classes have to be annotated!
 * Data containing objects with purpose to be passed around are just that, simple POJOs.
 * In this category are:
 * - domain model objects - objects specific to your service only
 * - DTOs - Data Transfer Objects, data transferred from and to your service to the outside
 * - Entities - representing data in the database, although those are usually annotated to keep ORM configuration concise, localised, minimal...
 */
//If we had lombok in our dependencies, this class would just be field definitions and lomboks @Data annotation
public class Book {
  private String name;
  private String author;

  public Book(String name, String author) {
    this.name = name;
    this.author = author;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  @Override
  public String toString() {
    return "Book{" +
      "name='" + name + '\'' +
      ", author='" + author + '\'' +
      '}';
  }
}
