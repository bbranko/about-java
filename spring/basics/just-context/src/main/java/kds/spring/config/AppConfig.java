package kds.spring.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.type.AnnotatedTypeMetadata;

import kds.spring.config.forInjectionDemo.Datasource;

//the core purpose of Configuration-annotated classes is to define @Bean methods
@Configuration

//Classes can have more than one annotation and purpose, ie for our basic setup, more than anything else
// we need to define where should Spring look for classes to manage in its context. This is done by
//ComponentScan, which by default starts to look form current package recursively down,
// but since we need to scan from root package, we can pass that in as configuration as so:
@ComponentScan("kds.spring")
public class AppConfig {

  //Bean annotation gives us an easy way to define a factory method for a particular class.
  @Bean
  public Datasource getDatasource() {
    //here we can put custom init logic, for the object being constructed
    //If we do not have one, it is usually cleaner to just annotate the class with the appropriate @Component subclass
    String customInitData = "custom datasource data";
    return new Datasource(customInitData);
  }

  //we can define more beans of the same type, but should we wish to inject them somewhere, we either have to:
  // - inject them as a collection
  // - or somehow specify which one should be injected
  //...a way do define a default(primary) implementation is to mark it with:
  @Primary
  @Bean
  public Datasource getPrimaryDatasource() {
    String customInitData = "primary datasource data";
    return new Datasource(customInitData);
  }

  //another way to differentiate beans is to define a Qualifier, and then refer to it at the point of injection
  @Qualifier("SpecificDatasource")
  @Bean
  public Datasource getSpecificDatasource() {
    String customInitData = "specific datasource data";
    return new Datasource(customInitData);
  }

  //we can conditionally create a bean based on Profile - in this case this bean will be created only if 'dev' profile is active
  @Profile("dev")
  @Bean
  public Datasource getDevDatasource() {
    String customInitData = "primary datasource data";
    return new Datasource(customInitData);
  }

  //Finally, there is also @Conditional to which arbitrary condition can be passed. Bean will be created only if the condition evaluates to 'true'.
  //With spring-boot, we also get a lot of helper derived conditionals due to the nature of auto-configurations...
  @Conditional(value = AlwaysTrueCondition.class)
  @Bean
  public Datasource getConditionlDatasource() {
    String customInitData = "conditional datasource data";
    return new Datasource(customInitData);
  }
}


//just for demo purposes
class AlwaysTrueCondition implements Condition {

  @Override
  public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
    System.out.println("AlwaysTrueCondition: Condition for bean creation checked!");
    return true;
  }
}
