package kds.spring.config.forInjectionDemo;

import org.springframework.stereotype.Component;

@Component
public class OneDatasourceManager {

  public OneDatasourceManager(Datasource myDatasource) {
    //Storing data is obsolete since we are only using this "Manager" classes for injection wiring demo:
    System.out.println("OneDatasourceManager: " + myDatasource.getData());
  }

}
