package kds.spring.dao;

import org.springframework.stereotype.Repository;

import java.util.List;
import kds.spring.model.Book;

/**
 * DAO is short for Data Access Object. It is a design pattern that is basically an Adapter or a Proxy to some persistence mechanism, usually a database.
 * Instead of DAO, other common extensions are Repository, DB as in DataBase or JPA as in JavaPersistenceAPI
 * <p>
 * We should really be implementing some sort of CRUD (Create,Read,Update,Delete) interface, but this is a demo.
 */
@Repository
public class BookDao {

  //simulation for demo purposes
  private List<Book> booksInDb;

  public BookDao() {
    //just for demo purposes, we will stub a database with a list
    this.booksInDb = List.of(
      new Book("1984", "Orwell"),
      new Book("I, Robot", "Asimov")
    );
  }

  //usual method that returns all persisted results
  public List<Book> findAll() {
    return booksInDb;
  }
}
