# Spring AOP

In its basic form, spring uses JDK dynamic proxy and CGLIB for purposes of the framework.
While it is possible to utilize it, it is cumbersome, limited and non-performant.
To add insult to injury, it is XML-based, or at least I did not manage to get it to work
with annotations. More investigation is required...

Spring has out-of-box full support for AspectJ, BUT dependency has to be added and
the feature has to be activated. AspectJ is non-trivially more complicated under the hood
than limited proxy-based AOP of core spring. For it to work, it either has to be passed
through custom AspectJ compiler or configure it to do LoadTimeWeaving — while this is
usually under-the-hood detail, I imagine that this process can error out due to dev-errors
with atypical error messages.

Other than that, AspectJ is the way to go - it is performant, fully supports all pointcuts
and can be fully configured and defined with classes and annotations.

Note: both approaches can be mixed.


## For bonus points:

Spring can be configured to use only AspectJ even for all its interval purposes.
Since AspectJ is much faster than Springs proxy-based approach, it would be nice to confirm
how to set it up and its benefits.
