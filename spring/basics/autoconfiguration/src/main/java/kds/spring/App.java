package kds.spring;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import kds.spring.config.PropertiesConfig;
import kds.spring.service.LibraryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//this single annotation:
// - sets up ComponentScan starting from here with some "excludes"
// - enables AutoConfigurations
// - sets class as Configuration
@SpringBootApplication
public class App {

  //we can now easily define a logger for a class, which becomes simpler still with lombok (@Slf4j annotation on a class)
  private static final Logger log = LoggerFactory.getLogger(App.class);
  //Benefits of a logger:
  // - it adds different levels of logging (trace, debug, info, warn, error) which can then be filtered upon
  // - it can easily be configured to:
  //   - add style and additional information around a basic message passed to logger
  //   - add multiple outputs(writers) with different behaviours
  // Loggers are usually configured via property files. Furthermore, spring can apply different ones depending on the active profile.
  // Note: usually each logger requires its own configuration file with its own syntax

  public static void main(String[] args) {
    log.info("Starting application, logging with: " + log.getClass().getName());
    //We again have to start up the framework, but this time we use SpringApplication.run, which also results in context being created.
    //Biggest difference being that now, processing of auto-configurations is part of initialization operations.
    ConfigurableApplicationContext context = SpringApplication.run(App.class);
    //just because we can, we won't do any manual bean getting...
  }

  //...we will instead define CommandLineRunner bean, which will be autowired and run for us
  // when a framework is nice and ready for it
  @Bean
  public CommandLineRunner libraryCmd(LibraryService libraryService) {
    log.info("LibraryCmd created...");
    return args -> {
      log.info("LibraryCmd started...");
      libraryService.printoutBooks();
    };
  }

  //of course, we can define more than one command line runner :D
  @Bean
  public CommandLineRunner propertyCmd(PropertiesConfig properties) {
    log.info("PropertyCmd created...");
    return args -> {
      log.info("PropertyCmd started...");

      log.info("Auto-mapped property: " + properties.getMyCustomAutoMappedProperty());
      log.info("@Value property: " + properties.getMyCustomPropertyViaAnnotation());
    };
  }

}
