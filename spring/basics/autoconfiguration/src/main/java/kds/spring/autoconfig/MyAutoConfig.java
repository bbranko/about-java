package kds.spring.autoconfig;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//autoconfiguration is just a configuration, usually paired with Conditional on a whole class
@Configuration
//BUT! it has to be also marked with @AutoConfiguration annotation!!!
// Otherwise, it will execute regardless if it is registered as autoconfiguration or not.
@AutoConfiguration
public class MyAutoConfig {
  private final Logger log = LoggerFactory.getLogger(MyAutoConfig.class);

  public MyAutoConfig() {
    log.warn("Thing to note is that auto-configurations get triggered LAST!");
  }

  @Bean
  //this is a big part of spring-boot and conditionals come in many flavors!
  @ConditionalOnMissingBean(Datasource.class)
  public Datasource getDatasource() {
    log.info("Autoconfiguration on missing bean triggered, creating missing bean.");
    String customInitData = "autoconfigured datasource data";
    return new Datasource(customInitData);
  }
}
