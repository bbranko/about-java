package kds.spring.service;

import org.springframework.stereotype.Service;

import kds.spring.dao.BookDao;
import kds.spring.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class LibraryServiceImpl implements LibraryService {
  //now that we have loggers, let's use them :)
  private final Logger log = LoggerFactory.getLogger(LibraryServiceImpl.class);

  private final BookDao bookDao;

  public LibraryServiceImpl(BookDao bookDao) {
    this.bookDao = bookDao;
  }

  @Override
  public void printoutBooks() {
    for (Book book : bookDao.findAll()) {
      log.info(book.toString());
    }
  }
}
