package kds.spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

//we still need Component annotation for class to register as bean
@Component
//but now we have an annotation to apply automapping from properties files - just pass a prefix!
//we can also set if loading should fail when to little or to many properties are defined under the provided prefix.
@ConfigurationProperties("my.custom.property")
//further note that we did not have to register PropertySource!
// this is because application and application-envName .properties and .yaml files get autoloaded
// more info on order and location: https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.external-config
public class PropertiesConfig {

  //this will now get auto-mapped. CAUTION: Getters and setters must be defined! (lombok really helps here)
  private String value;

  //we can still have this, but then again we can have this in any managed bean ¯\_(ツ)_/¯
  @Value("${my.custom.property.mapped.by.annotation}")
  private String annotatedProperty;

  public String getValue() {
    return value;
  }

  //needed for automapping to work...
  public void setValue(String value) {
    this.value = value;
  }

  public String getMyCustomAutoMappedProperty() {
    return value;
  }

  public String getMyCustomPropertyViaAnnotation() {
    return annotatedProperty;
  }
}
