package kds.spring.autoconfig;

//At times, we may not want, or can register class for Spring DI with annotations.
//In that case, we can always do it with through @Configuration class and its @Bean method
public class Datasource {
  private String data;

  public Datasource(String data) {
    this.data = data;
  }

  public String getData() {
    return data;
  }
}
