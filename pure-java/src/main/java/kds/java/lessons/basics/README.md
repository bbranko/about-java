# Lessons about Java basics

Before starting with Java lessons, general overview should be presented, ie. info/overview.png.
Afterward, basic setup of development environment should be verified and a minimal set of expected
commands and shortcuts to know should be communicated for ease of following and participation,
ie. info/dev_env_setup.txt

Suggested order of going through topics:

- Syntax
- PrimitiveTypes
- ControlStatements
  - good place for an easy task

> to give a breather, and if needed, **Git** basics can be covered here

- DataStructures
  - good place for a harder task

> to give another breather and to allow for more time while working on a harder task
> **Gradle** basics can be presented here

- TDD methodology presentation
  - now easy to showcase, since we have conventional gradle project set up,
    and we can easily add JUnit library
  - use it to confirm understanding of previous knowledge
  - previous task can be used for demo since it is familiar
    - it can also double as a review of solution to a previous task
- AboutAnnotations (need for Spring)
- AboutExceptions

These topics wrap up basics pretty nicely, continuing, the following topics come to mind:

- FileSystem I/O, nice precursor to reading configuration files
- Basics about threads and thread safety
  - it is nice to have it in mind, buuut: 
  - concurrency is really cumbersome to present in a tangible way...
    - at minimum, synchronized should be covered
    - due to Selenium, Thread .sleep - but with a disclaimer that this is a busy wait at heart...
    - maybe also, Thread .start .join and/or Object .wait .notify and .notifyAll
    - some form of visualization would be fantastic
    - IMO, anything after Thread .sleep is already too much and topics get complex fast from there on
  - clarification for terms "concurrently" and "in parallel" should also be presented...
    - good visualisation: https://www.educative.io/answers/concurrent-vs-parallel-programming
- Streams API, and through it FunctionalInterfaces and Lambdas
