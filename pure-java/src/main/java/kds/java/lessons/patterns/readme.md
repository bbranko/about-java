# Design patterns

All "Gang of four" patterns are covered, suggested order:

1. creational
2. structural
3. behavioural

At minimum, for framework related discussions such as about spring, at minimum cover:

- Template Pattern, because it perfectly illustrates what framework actually is
- Dependency injection, just to clarify what that simple concept is
- Bean, just to clarify since it is often mentioned
- Singleton, since most of the instantiated objects by the framework are singletons

Optionals and nice to haves:

- Adapter and Facade, since those are the ones most used when dealing with legacy
- Flyweight(Cache), since it is often important to make calls more performant
- Factory pattern, since that is how frameworks usually instantiate objects
- Proxy, since some object types tend to be wrapped in them
- Chain of responsibility, since that is usually how configuration is loaded and security configured

Finally, Mediator, Observer and Visitor patterns are of grand help as ideas for better upfront, or
as a refactoring designs.
