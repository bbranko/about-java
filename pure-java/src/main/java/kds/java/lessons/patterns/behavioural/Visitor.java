package kds.java.lessons.patterns.behavioural;

import java.util.List;

/**
 * The Visitor design pattern provides a way of separating algorithm from object structure on which it operates.
 * An algorithm that visitor implements is often disjointed from the context of objects being visited.
 * Visitor achieves its goal by means of double dispatch.
 * <p>
 * Double dispatch is the idea of polymorphically calling a method within which we polymorphically call another method.
 * Concretely, we polymorphically call .accept method of a visitable object, within which we polymorphically call
 * .visit method of a passed in visitor passing this as an argument.
 * <p>
 * We are using it in such a roundabout way, as opposed to just calling method directly with an object as an argument,
 * to preserve dynamic linking. Namely, if we just call .visit method on an object, we may wrongly invoke .visit method
 * of a superclass in which the object was stored at the time of invocation. With double dispatch, we first call .accept
 * method of the object witch polymorphically resolves to a method for that type of object; within that method, "this" is
 * always of that object's type (never superclass or sth else); thus, when we finally call .visit(this) we will always
 * polymorphically call correct .visit method!
 * <p>
 * Note: one possible downside to Visitor is that its interaction with passed in objects is limited to objects public API.
 */
public class Visitor {

  //example usage
  public static void main(String[] args) {
    //example of the visitor pattern with classes in a hierarchy,
    //but first lets confirm that just invoking .visit method directly will not work:
    //...for example, if we have a list of shapes containing generic shapes as well as specialized ones
    List<Shape> shapes = List.of(
      new Shape(),
      new Circle(),
      new Rectangle()
    );

    //also, we instantiate our visitor - the nice thing here is that with pattern in place we can add as many concrete implementations as we want!
    VisitorInterface sayNameVisitor = new VisitorWithOverloadedMethods();

    //...if we now invoke .visit method on list elements directly:
    for(Shape shape : shapes){
      //we will notice that even though shapes are mix of shape and specialized classes, due to the static linking,
      //only the most generic handler (one for shapes) will ever be invoked!
      sayNameVisitor.visit(shape);
    }

    System.out.println("--");

    //...if however, we first invoke polymorphic call to .accept method
    for(Shape shape : shapes){
      //we will notice that our subsequent call to .visit method will be correctly resolved to handler for a correct type
      shape.accept(sayNameVisitor);
      //this two-step polymorphic call is what double dispatch is!
    }
  }
}

//To start, we need an entry method at the place of invocation - for this polymorphism always works :)
interface Visitable {
  void accept(VisitorInterface visitor);
}

//but imagine we want to visit a group of objects from different levels of a hierarchy, ie:
class Shape implements Visitable {
  //...
  void sayMyName() {
    System.out.println("am a generic shape!");
  }
  @Override
  public void accept(VisitorInterface visitor) {
    visitor.visit(this);
  }
  //...
}
class Circle extends Shape {
  //...
  @Override
  void sayMyName() {
    System.out.println("am a circle!");
  }
  //IMPORTANT! even though implementation is the same as in the base class, we need this concrete implementation
  //for double dispatch to work! That is because "this" from base class is a different type(base type)
  //than "this" from our subclass!
  @Override
  public void accept(VisitorInterface visitor) {
    visitor.visit(this);
  }
  //...
}
class Rectangle extends Shape {
  //...
  void sayMyName() {
    System.out.println("am a rectangle!");
  }
  //see explanation why this overridden method is needed.
  //comment this method out and see the difference for yourself.
  @Override
  public void accept(VisitorInterface visitor) {
    visitor.visit(this);
  }
  //...
}

//...then we can relly on overloading to resolve which method to call.
//BUT! only if we provide the right object type at the time of invocation!
interface VisitorInterface {
  void visit(Shape shape);
  void visit(Circle circle);
  void visit(Rectangle rectangle);
}


class VisitorWithOverloadedMethods implements VisitorInterface {
  @Override
  public void visit(Shape shape) {
    System.out.println("Working with shape");
    shape.sayMyName();
  }

  @Override
  public void visit(Circle circle) {
    System.out.println("Working with circle");
    circle.sayMyName();
  }

  @Override
  public void visit(Rectangle rectangle) {
    System.out.println("Working with rectangle");
    rectangle.sayMyName();
  }
}


