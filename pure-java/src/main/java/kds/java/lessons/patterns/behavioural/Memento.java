package kds.java.lessons.patterns.behavioural;

import java.util.Stack;

/**
 * Memento is a pattern that defines how to create and save state snapshot of another object with full encapsulation.
 * In other words, the purpose of memento is to allow only an originator object to create a memento and restore its
 * state from memento. No one else should be able to access or modify state saved within memento, but other classes can,
 * for example, manage a collection of mementos to serve as an undo list.
 * <p>
 * In usual setup Originator creates Mementos(state snapshots) and Caretaker stores and manages a collection of mementos.
 */
public class Memento {

  //example usage
  public static void main(String[] args) {
    //Memento as inner class of originator, for purposes of demo we will not use caretaker...
    OriginatorWithInner originatorInner = new OriginatorWithInner();
    originatorInner.setState("initial state");

    //note that we cannot interact with the contents of the memento in any way
    // well at least not in any regular manner - ie. we could still modify it through reflection API...
    OriginatorWithInner.Memento preservedStateInner = originatorInner.createSnapshot();
    originatorInner.setState("new state");

    System.out.println("Current state: " + originatorInner.getState());
    //...yet the originator can still use memento to restore its state!
    originatorInner.restore(preservedStateInner);
    System.out.println("Restored state: " + originatorInner.getState());

    System.out.println("--");

    //Memento isolated via interface - less secure than inner class, since we can peek at inner state with cast,
    // but then again the purpose is clear and memento is still immutable
    OriginatorWithOuter originatorOuter = new OriginatorWithOuter();
    originatorOuter.setState("initial state");

    //This is the only difference to previous example:
    // - before, we had state stored in concrete class that we could not access.
    // - now, we store it in interface with no methods exposed, and we still cannot do anything with it.
    //Well until we cast it into a proper class, but that would be misuse of a pattern...
    MementoInterface preservedStateOuter = originatorOuter.createSnapshot();
    originatorOuter.setState("new state");

    System.out.println("Current state: " + originatorOuter.getState());
    //...yet the originator can still use memento to restore its state!
    originatorOuter.restore(preservedStateOuter);
    System.out.println("Restored state: " + originatorOuter.getState());

  }
}

//Memento as inner class of originator
class OriginatorWithInner {
  //of course inner state can be far more complex...
  private String state;

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  static class Memento {
    //instead of preserving field values, for simple cases, memento can store state as a clone of a whole originator via a Prototype pattern
    private final String preservedState;

    public Memento(String preservedState) {
      this.preservedState = preservedState;
    }
  }

  public Memento createSnapshot() {
    return new Memento(state);
  }

  public void restore(Memento someSnapshot) {
    //this works because outer class can access private fields of its inner class and no one else can :)
    state = someSnapshot.preservedState;
  }
}

//Memento hiding inner state via interface
interface MementoInterface {
}

//since Mementos are meant to be immutable state snapshots, new Java records are the perfect fit for it!
record MementoForOriginator(String stateSnapshot) implements MementoInterface {
};

//we still need an originator to create and restore
class OriginatorWithOuter {
  private String state;

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public MementoInterface createSnapshot() {
    return new MementoForOriginator(state);
  }

  public void restore(MementoInterface someSnapshot) {
    //>>> mySnapshot here is a pattern variable! We can use this auto cast enchantment to instanceof since Java 14!
    if (someSnapshot instanceof MementoForOriginator mySnapshot) {
      //we can take this even further and first preserve than check if we were an original memento owner!
      state = mySnapshot.stateSnapshot();
    } else {
      System.out.println("Error: not my snapshot!");
    }
  }
}
