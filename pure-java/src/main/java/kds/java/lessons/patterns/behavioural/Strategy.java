package kds.java.lessons.patterns.behavioural;


import java.util.List;

/**
 * Strategy pattern defines how to change behavior(strategy) of one method at runtime.
 * <p>
 * For example:
 * - to change navigation generateRoute method from "for car" to "for walking"
 * - or to change calculator calculate method from "add 2 numbers" to "subtract 2 numbers"
 */
public class Strategy {
  //example code
  public static void main(String[] args) {
    NavigationStrategy carRouteStrategy = new CarRouteStrategy();
    NavigationStrategy walkRouteStrategy = new WalkRouteStrategy();

    //ie. we create our navigator with initial strategy set to routes for cars
    NavigationInterface navigator = new Navigator(carRouteStrategy);
    navigator.navigate();
    //at some point, we switch to walking
    navigator.setRoutingStrategy(walkRouteStrategy);
    navigator.navigate();


    System.out.println("Example usage of strategy pattern in Java 8+");
    //Starting with Java 8 and addition of Stream API, there are far more practical examples of applying a strategy pattern,
    //namely, lambdas that we are passing to stream processing steps such as .map, .filter, etc. are prime examples of application of
    // the Strategy pattern!
    List<Integer> exampleList = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
    exampleList.stream()
      .filter(number -> number % 2 == 0) //setting a custom filtering strategy to remove all odd numbers
      .map(number -> number * number)    //setting a custom map strategy to square all numbers
      .forEach(System.out::println);     //setting a custom consumer strategy to print out all numbers

    //when used properly, a strategy pattern can be very powerful due to the flexibility it adds and simplicity of its usage!
  }
}

//note
interface NavigationStrategy{
  String calculateRoute();
}

class CarRouteStrategy implements NavigationStrategy{
  @Override
  public String calculateRoute() {
    return "calculating route for CAR done.";
  }
}
class WalkRouteStrategy implements NavigationStrategy{
  @Override
  public String calculateRoute() {
    return "calculating route for WALKING done.";
  }
}

interface NavigationInterface {
  //for strategy pattern, we should have the ability to change strategy in runtime
  void setRoutingStrategy(NavigationStrategy strategy);
  //...and a way to trigger execution of current strategy
  void navigate();
}

class Navigator implements NavigationInterface {
  NavigationStrategy currentStrategy;

  public Navigator(NavigationStrategy initialStrategy) {
    this.currentStrategy = initialStrategy;
  }

  @Override
  public void setRoutingStrategy(NavigationStrategy strategy) {
    this.currentStrategy = strategy;
  }
  @Override
  public void navigate() {
    System.out.println(currentStrategy.calculateRoute());
  }
}
