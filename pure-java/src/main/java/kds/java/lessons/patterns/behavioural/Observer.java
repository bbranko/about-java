package kds.java.lessons.patterns.behavioural;

import java.util.ArrayList;
import java.util.List;

/**
 * The Observer pattern defines a way of how objects get notified about a change that they are interested in.
 * Inefficient way would be, for example, a periodic busy-polling of an object asking if some change has happened.
 * A much better way is to let an object subscribe to object being observed and let it emit an event to all observers
 * when a change of interest happens.
 * <p>
 * This idea of subscribing to an object to be notified is the reason why the Observer pattern is also known
 * as the Publisher/Subscriber pattern.
 */
public class Observer {
  //example code
  public static void main(String[] args) throws InterruptedException {
    Observable observable1 = new Observable();
    BusyWaitObserver badObserver = new BusyWaitObserver();
    Thread observableDoingWork1 = getObservableDoingWork(observable1);
    observableDoingWork1.start();

    while (true) {
      sleep(100);
      boolean isConditionMet = badObserver.checkByPolling(observable1);
      if (isConditionMet) {
        badObserver.doWork();
        break;
      }
    }

    //cleanup after thread execution - not a part of example
    observableDoingWork1.join();

    System.out.println("--");

    //with proper observer(subscriber) we offload responsibility to Observable(publisher) to notify us of the desired event.
    Observable observable2 = new Observable();
    ProperObserver properObserver = new ProperObserver();
    observable2.addObserver(properObserver); //we could also pass here an event type, or condition for Observable to notify an Observer

    Thread observableDoingWork2 = getObservableDoingWork(observable2);

    observableDoingWork2.start();
    //no while, no polling, no nothing... Observable will notify ProperObserver at appropriate time - THE END!

    //cleanup after thread execution - not a part of example
    observableDoingWork1.join();
  }


  //Just to simulate observable changing in an independent way from Observer
  private static Thread getObservableDoingWork(Observable observable) {
    return new Thread(() -> {
      do {
        //simulate work
        observable.doSomething();
        sleep(100);
        //just so that we do not keep threads running in infinite loop until the main end.
      } while (!observable.isDone());
    });
  }

  //exceptionless sleep, just to simplify demo code...
  public static void sleep(int ms) {
    try {
      Thread.sleep(ms);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}

//or a Publisher
class Observable {
  int counter = 0;
  boolean done = false;
  List<ProperObserver> observers = new ArrayList<>(); //...or subscribers
  //if our class could emit many different events, we would have more than one list.

  void doSomething() {
    if (Math.random() > 0.6) {
      //just to give some randomness to delay of work being done
      counter++;
    }
    System.out.println("doing something... done: " + counter);

    //Observable(Publisher) would have places set up at various places to notify observers(subscribers) of various events
    //for demo purposes, we will only emit one event. We will notify subscribed observers once counter reaches 5:
    if (counter == 5) {
      for (ProperObserver observer : observers) {
        observer.handleEvent();
      }
      //not part of the pattern - this is just so that thread of the Observable ends;
      done = true;
    }
  }

  public int getCounter() {
    return counter;
  }

  public boolean isDone() {
    return done;
  }

  //part of the pattern:
  //or add/remove Subscriber
  public void addObserver(ProperObserver observer) {
    observers.add(observer);
  }

  public void removeObserver(ProperObserver observer) {
    observers.remove(observer);
  }
  //--
}

//BusyWaitObserver is not only bad due to busy waiting, but it is also coupled to Observable since it has to know
//how to check it if event condition has been met when in reality it should be responsibility of Observable to know
//when event triggers...
class BusyWaitObserver {
  boolean checkByPolling(Observable observable) {
    System.out.println("Busy checking again if Observable has met my condition...");
    return observable.getCounter() >= 5;
  }

  void doWork() {
    System.out.println("Finally doing work after BUSY busy wait...");
  }
}

class ProperObserver {
  boolean done = false;

  //part of the pattern; usually named "notify", but it cannot be in java due to Object.notify()
  void handleEvent() {
    //here we handle emitted event; note that we could have passed in parameters etc... but all that is done on case-by-case basis.
    System.out.println("Proper observer handling event in response to notification from observable");
    done = true;
  }
  //--

  public boolean isDone() {
    return done;
  }
}
