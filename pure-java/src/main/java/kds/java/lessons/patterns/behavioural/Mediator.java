package kds.java.lessons.patterns.behavioural;

import java.util.function.Consumer;

/**
 * The Mediator pattern is the idea of centralizing interaction know-how in complex structure.
 * <p>
 * Instead of each object knowing with which other object it needs to interact and how, we can connect them all to
 * one central Mediator(Orchestrator) which will mediate(orchestrate) further object interaction.
 */
public class Mediator {
  //example usage
  public static void main(String[] args) {
    //note that all changes are under the hood - see implementation to better understand the pattern
    //we will simulate a same set of user interaction with user class
    User user = new User();

    SpaghettiForm spaghettiForm = new SpaghettiForm();

    user.enterData(spaghettiForm.getTextField1()::enterData, "field textfield 1");
    user.click(spaghettiForm.getSubmitButton()::click);
    user.enterData(spaghettiForm.getTextField2()::enterData, "field textfield 2");
    user.click(spaghettiForm.getSubmitButton()::click);

    System.out.println("--");

    MediatedForm mediatedForm = new MediatedForm();

    //note, when using Mediator, it can also serve as a facade for inner elements
    user.enterData(mediatedForm::enterTextField1, "field textfield 1");
    user.click(mediatedForm::clickSubmit);
    user.enterData(mediatedForm::enterTextField2, "field textfield 2");
    user.click(mediatedForm::clickSubmit);
  }
}

//we will use User class just to simulate user interaction
class User {
  //this method simulates user actually entering data into textfield of a form
  public void enterData(Consumer<String> consumer, String data) {
    consumer.accept(data);
  }

  //this method simulates user triggering a button
  //We use Runnable just because its functional interface fits our contract, so that we do not need to define our own.
  public void click(Runnable clickToTrigger) {
    clickToTrigger.run();
  }
}

//Spaghetti implementation
class SpaghettiForm {
  SpaghettiTextField textField1;
  SpaghettiTextField textField2;
  SpaghettiSubmitButton submitButton;

  //at first this seems like an easier way to go about building things
  public SpaghettiForm() {
    textField1 = new SpaghettiTextField();
    textField2 = new SpaghettiTextField();
    submitButton = new SpaghettiSubmitButton();

    textField1.initialize(textField2, submitButton);
    textField2.initialize(textField1, submitButton);
    submitButton.initialize(textField1, textField2);
  }

  public SpaghettiTextField getTextField1() {
    return textField1;
  }

  public SpaghettiTextField getTextField2() {
    return textField2;
  }

  public SpaghettiSubmitButton getSubmitButton() {
    return submitButton;
  }
}

//...buuut with the first few changes you will quickly realize what an unmaintainable mess this style actually is
class SpaghettiTextField {
  SpaghettiTextField otherTextField;
  SpaghettiSubmitButton submitButtonToEnable;

  String myData;

  public void initialize(SpaghettiTextField otherTextField, SpaghettiSubmitButton submitButtonToEnable) {
    this.otherTextField = otherTextField;
    this.submitButtonToEnable = submitButtonToEnable;
  }

  public void enterData(String data) {
    myData = data;
    System.out.println("TextField - data entered...");

    System.out.println("TextField - checking rest of the form...");
    if (otherTextField.isValid()) {
      System.out.println("TextField - rest of form valid enabling Submit...");
      submitButtonToEnable.enable();
    }
  }

  public String getData() {
    return myData;
  }

  public boolean isValid() {
    return myData != null;
  }
}

class SpaghettiSubmitButton {
  SpaghettiTextField textField1;
  SpaghettiTextField textField2;

  boolean isEnabled = false;

  public void initialize(SpaghettiTextField textField1, SpaghettiTextField textField2) {
    this.textField1 = textField1;
    this.textField2 = textField2;
  }

  public void enable() {
    isEnabled = true;
  }

  public void click() {
    if (!isEnabled) {
      System.out.println("SubmitButton - inactive, fill out the form first!");
      return;
    }

    System.out.println("SubmitButton - getting data...");
    System.out.println("SubmitButton - submitting data: " + textField1.getData() + ", " + textField2.getData());
  }
}
//Note that, in spaghetti(interwoven) codebase it very easy to miss something and introduce non-trivial bugs.
//Spaghetti implementation also violates single responsibility principle :)


//Mediator implementation
class MediatedForm {
  MediatedTextField textField1;
  MediatedTextField textField2;
  MediatedSubmitButton submitButton;

  //note how elements are only coupled to mediator as opposed to each other!
  public MediatedForm() {
    textField1 = new MediatedTextField(this);
    textField2 = new MediatedTextField(this);
    submitButton = new MediatedSubmitButton(this);
  }

  //mediator acting as facade here
  public void enterTextField1(String data) {
    textField1.enterData(data);
  }

  public void enterTextField2(String data) {
    textField2.enterData(data);
  }

  //only mediator knows and can act on the full picture, this keeps logic centralized
  //and constituent elements decoupled!
  public void validateForm() {
    if (textField1.isValid() && textField2.isValid()) {
      System.out.println("Form - form validated.");
      submitButton.enable();
    } else {
      System.out.println("Form - form not yet fully filled...");
    }
  }

  public void clickSubmit() {
    if (!submitButton.isEnabled()) {
      System.out.println("Form - Button is inactive, fill the form first...");
      return;
    }

    submitButton.click();
  }

  public void sendData() {
    System.out.println("Form - getting data...");
    System.out.println("Form - submitting data: " + textField1.getData() + ", " + textField2.getData());
  }
}

class MediatedTextField {
  //usually is the owner or instantiator of the object itself
  MediatedForm mediator;
  String myData;

  public MediatedTextField(MediatedForm mediator) {
    this.mediator = mediator;
  }

  //now each element is only concerned with its own operations and the rest is delegated to mediator!
  public void enterData(String data) {
    myData = data;
    System.out.println("TextField - data entered, asking mediator to validate form...");
    mediator.validateForm();
  }

  public String getData() {
    return myData;
  }

  public boolean isValid() {
    return myData != null;
  }
}

class MediatedSubmitButton {
  MediatedForm mediator;
  boolean enabled = false;

  public MediatedSubmitButton(MediatedForm mediator) {
    this.mediator = mediator;
  }

  //now each element is only concerned with its own operations and the rest is delegated to mediator!
  public void enable() {
    enabled = true;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void click() {
    if (!enabled) {
      System.out.println("SubmitButton - ERROR, button not enabled!");
      return;
    }

    System.out.println("SubmitButton - notifying mediator to send data");
    mediator.sendData();
  }
}
