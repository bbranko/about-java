package kds.java.playground;

import java.util.HashSet;
import java.util.Set;

/**
 * Present here are just some of the most basic examples to point in right direction when thinking of memory leaks.
 * <p>
 * Demo should be run with -Xmx restriction to a small amount, ie 50~100MB.
 * And for best effect usage of tool such as VisualVM[1] is advised to get realtime insight in JVM memory allocation.
 * <p>
 * [1] https://visualvm.github.io
 */
public class MemoryLeak {

  static Set<Node> set = new HashSet<>();

  public static void main(String[] args) {

    //Java is much more resilient than it was \o/

    //String pool is in heap and GCable
//    while (true) {
//      String s = String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
//      s.intern();
//    }

    //Simple cyclic references are caught and collectable
//    while (true) {
//      Node n1 = new Node();
//      Node n2 = new Node();
//      Node n3 = new Node();
//      n1.n = n2;
//      n2.n = n3;
//      n3.n = n1;
//    }

    //Doing stupid things will still cause OutOfMemory though
//    while (true) {
//      set.add(new Node());
//    }
  }
}

class Node {
  Node n;
}

