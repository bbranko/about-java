# Java Playground

Package intended as place to drop in small code snippets demonstrating or testing a single aspect of Java.
A collection of under-defined scratches, a playground.

In time, a topic of a snippet could be promoted to a lesson or converted and better covered via test suite.
