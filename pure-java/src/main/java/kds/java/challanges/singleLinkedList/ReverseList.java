package kds.java.challanges.singleLinkedList;

//Given head(root) of a singly linked list, return head of a reversed list;
public class ReverseList {
  public static void main(String[] args) {
    ListNode node5 = new ListNode(5, null);
    ListNode node4 = new ListNode(4, node5);
    ListNode node3 = new ListNode(3, node4);
    ListNode node2 = new ListNode(2, node3);
    ListNode root = new ListNode(1, node2);

    ListNode node = root;
    do {
      System.out.print(node.data + " ");
      node = node.next;
    } while (node != null);

    System.out.println("\n-- reversed --");

    node = reverseList(root);
    do {
      System.out.print(node.data + " ");
      node = node.next;
    } while (node != null);

  }

  //it is basically a three-way value swap (as opposed to regular two)
  public static ListNode reverseList(ListNode head) {
    ListNode newHead = null;    //init previous to null

    while (head != null) {
      ListNode next = head.next;//save next node
      head.next = newHead;      //rewire current.next to previous element
      newHead = head;           //move the previous element by one by setting its value to current
      head = next;              //set the next node as next current
    }
    //if the .next node became null, it means we reached the end of the list, and previous is newHead! :)

    return newHead;
  }
}
