package kds.java.challanges.singleLinkedList;

//get negative index from SingleLinkedList

public class GetNegativeIndex {

  public static void main(String[] args) {
    ListNode node5 = new ListNode(5, null);
    ListNode node4 = new ListNode(4, node5);
    ListNode node3 = new ListNode(3, node4);
    ListNode node2 = new ListNode(2, node3);
    ListNode node1 = new ListNode(1, node2);

    for (int i = 0; i < 6; i++) {
      ListNode result = getFromNegativeIndex(node1, i);
      System.out.println("Node at -" + i + ": " + result);
    }
  }

  static ListNode getFromNegativeIndex(ListNode listHead, int negativeIndex) {
    ListNode result = listHead;
    while (listHead.next != null) {
      if (negativeIndex > 0) {
        negativeIndex--;
      } else {
        result = result.next;
      }

      listHead = listHead.next;
    }

    if (negativeIndex > 0) {
      System.out.println("Error - index out of bounds");
      return null;
    } else {
      return result;
    }
  }
}
