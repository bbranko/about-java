package kds.java.challanges.singleLinkedList;

/**
 * SingleLinkedList skeleton implementation
 */
public class ListNode {
  int data;
  ListNode next;

  public ListNode(int data, ListNode next) {
    this.data = data;
    this.next = next;
  }

  @Override
  public String toString() {
    return "ListNode{" +
      "data=" + data +
      ", next->" + nextToString() +
      '}';
  }

  private String nextToString() {
    if (next != null) {
      return "(" + next.data + ")->" + next.nextToString();
    } else {
      return null;
    }
  }
}
