package kds.java.challanges.misc;

//Given a non-empty array of ints, every int appears twice except for one. Find that single one.
//Solution has to run in O(1) and only use one more variable.

// XOR, one pass through an array and xor operation will filter out duplicates and produce the desired solution
// in the desired time and space...
public class FindUnpairedNumber {

  public static void main(String[] args) {
    System.out.println("3:" + singleNumber(new int[]{1,1,2,2,3}));
    System.out.println("3:" + singleNumber(new int[]{1,2,3,1,2}));
  }

  public static int singleNumber(int[] nums) {
    int unpairedNum = 0;
    for (int num : nums){
      unpairedNum ^= num; // bitwise xor operator in java is: ^
    }
    return unpairedNum;
  }
}
