package kds.java.challanges.misc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

/*
  Given k servers which are servicing requests in round-robin fashion, and list of requests with their duration,
  return all server ids which serverd most requests.

  Requests are coming in every second if all servers are busy, request is dropped.
  https://leetcode.com/problems/find-servers-that-handled-most-number-of-requests/description/
 */
public class MostRequests {
  public static void main(String[] args) {

    List<List<Integer>> resutls = List.of(
      //leetcode examples
      busiestServers(3, new int[]{1, 2, 3, 4, 5}, new int[]{5, 2, 3, 3, 3}),
      busiestServers(3, new int[]{1, 2, 3, 4}, new int[]{1, 2, 1, 2}),
      busiestServers(3, new int[]{1, 2, 3}, new int[]{10, 12, 11}),

      //different time arrivals
      busiestServers(3, new int[]{1, 4, 5, 9}, new int[]{11, 5, 4, 1})
    );

    for (List<Integer> result : resutls) {
      System.out.println(result);
      System.out.println("--");
    }
  }


  static List<Integer> busiestServers(int k, int[] arrival, int[] load) {
    int[] requestCounter = new int[k];
    int[] busyUntil = new int[k];

    //handle requests
    for (int i = 0; i < arrival.length; i++) {
      boolean reqAssigned = false;

      for (int j = 0; j < k; j++) {
        int serverIndex = (i + j) % k;
        if (busyUntil[serverIndex] <= arrival[i]) {
          requestCounter[serverIndex] += 1;
          busyUntil[serverIndex] = arrival[i] + load[i];
          reqAssigned = true;
          break;
        }
      }

      if (!reqAssigned) {
        System.out.println("All servers are busy, request " + i + " dropped.");
      }
    }

    //find the busiest
    //tree set sorts via passed in comparator in logN time!
    TreeSet<Server> treeSet = new TreeSet<>(Comparator
      .comparingInt(Server::requestCounter)
      //if we compare only by reqCount, servers with the same as any previous one reqCount will not be added!
      .thenComparing(Server::id)
    );
    for (int i = 0; i < k; i++) {
      Server server = new Server(i, requestCounter[i]);
      treeSet.add(server);
    }

    List<Integer> busiestServers = new ArrayList<>();
    Iterator<Server> iterator = treeSet.descendingIterator();
    int previousRequestCount = -1;

    while (iterator.hasNext()) {
      Server server = iterator.next();

      if (server.requestCounter() >= previousRequestCount) {
        previousRequestCount = server.requestCounter();
        busiestServers.add(server.id());
      } else {
        break;
      }
    }

    return busiestServers;
  }

}

record Server(int id, int requestCounter) {
}


/*
  Example data:

  Input: k = 3, arrival = [1,2,3,4,5], load = [5,2,3,3,3]
  Output: [1]
  Explanation:
  All servers start out as available.
  The first 3 requests are handled by the first 3 servers in order.
  Request 3 comes in. Server 0 is busy, so it's assigned to the next available server, which is 1.
  Request 4 comes in. It cannot be handled since all servers are busy, so it is dropped.
  Servers 0 and 2 handled one request each, while server 1 handled two requests. Hence server 1 is the busiest server.

  Example 2:

  Input: k = 3, arrival = [1,2,3,4], load = [1,2,1,2]
  Output: [0]
  Explanation:
  The first 3 requests are handled by the first 3 servers.
  Request 3 comes in. It is handled by server 0 since the server is available.
  Server 0 handled two requests, while servers 1 and 2 handled one request each. Hence server 0 is the busiest server.

  Example 3:

  Input: k = 3, arrival = [1,2,3], load = [10,12,11]
  Output: [0,1,2]
  Explanation: Each server handles a single request, so they are all considered the busiest.
 */
