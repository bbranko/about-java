package kds.java.challanges.misc;

import java.util.List;
import java.util.Map;

/**
 * The roman numeral system consists of letters with the following values:
 * - I = 1
 * - V = 5
 * - X = 10
 * - L = 50
 * - C = 100
 * - D = 500
 * - M = 1000
 * Furthermore:
 * - I can precede only V or X and lowers their value by 1.
 * - X can precede only L or C and lowers their value by 10.
 * - C can precede only D or M and lowers their value by 100.
 * There should be at most only one presiding letter.
 * Any letter should repeat at most three times.
 * <p>
 * With full restrictions, roman numbers can cover a range of 1 to 3999.
 * Historically, Romans were more flexible.
 */
public class RomanNumberConversion {

  public static void main(String[] args) {
    for (int number : List.of(1893, 3999, 4000, 8, 4, 44)) {
      String roman = decimalToRomanViaConversionMap(number);
      String romanViaRadix = decimalToRomanViaMixedRadix (number);
      int numberFromRoman = romanToDecimalViaConversionMap(roman);
      int numberFromRomanViaRadix = romanToDecimalViaConversionMap(romanViaRadix);
      System.out.println("via conversion map: " + number + " : " + roman + " : " + numberFromRoman);
      System.out.println("via mixed radix: " + number + " : " + romanViaRadix + " : " + numberFromRomanViaRadix);
    }

  }

  //simpler approach to a key value map - alternative would be usage of an ordered map such as LinkedHashMap
  static final String[] LETTERS = new String[]{
    "M", "CM", "D", "CD",
    "C", "XC", "L", "XL",
    "X", "IX", "V", "IV",
    "I"
  };
  static final int[] VALUES = new int[]{
    1000, 900, 500, 400,
    100, 90, 50, 40,
    10, 9, 5, 4,
    1
  };

  static String decimalToRomanViaConversionMap(int number) {
    //end condition does not matter much since both letters and values have the same length
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < LETTERS.length; i++) {
      while (number >= VALUES[i]) {
        number -= VALUES[i];
        result.append(LETTERS[i]);
        //we can check here that no letter repeats more than three times...
      }
    }
    return result.toString();
  }

  //hashmap works better here, because we need to lookup values of letters
  static final Map<Character, Integer> LETTER_TO_VALUE = Map.of(
    'M', 1000, 'D', 500,
    'C', 100, 'L', 50,
    'X', 10, 'V', 5,
    'I', 1
  );

  static int romanToDecimalViaConversionMap(String roman) {
    //cleanup ops such as toUpper and trim should come here...
    int result = 0;
    for (int i = roman.length() - 1; i >= 0; i--) {
      char letter = roman.charAt(i);
      result += LETTER_TO_VALUE.get(letter);

      if (i - 1 >= 0) {
        //check if we need to subtract 1, 10 or 100
        char prevLetter = roman.charAt(i - 1);
        if (((letter == 'M' || letter == 'D') && prevLetter == 'C') ||
          ((letter == 'C' || letter == 'L') && prevLetter == 'X') ||
          ((letter == 'X' || letter == 'V') && prevLetter == 'I')
        ) {
          result -= LETTER_TO_VALUE.get(prevLetter);
          i--;
        }
      }

    }
    return result;
  }


  //200IQ solution with mixed radix - but it does not follow repeat letter max 3 times rule:
  static final char[] SINGLE_LETTERS = new char[]{'I', 'V', 'X', 'L', 'C', 'D', 'M'};

  static String decimalToRomanViaMixedRadix(int number) {
    StringBuilder result = new StringBuilder();

    //we start radix with 5 and bitwise XOR with 7, then oscillates radix value between 2 and 5.
    //letter tracks which letter we are currently working with.
    for (
      int radix = 5, letter = 0;
      number > 0;
      radix ^= 7, letter++
    ) {
      int repeatLetter = number % radix;
      while (repeatLetter-- > 0) {
        result.insert(0, SINGLE_LETTERS[letter]);
      }
      number = number / radix;
    }

    return result.toString();
  }
}
