package kds.java.challanges.slidingWindow;

/*
  Given a string, remove excess whitespace.
  Operation should be O(1) time and space complexity.

  O(1) space complexity is not possible in Java.
  Even when using 'char[]' we cannot resize it after manipulation.
  And, as always, strings in Java are immutable.
*/

import java.util.Arrays;
import java.util.List;

public class RemoveWhiteSpace {

  public static void main(String[] args) {
    String allWhitespace = " \t\n";
    String withTrailingSpace = "  this string   has trailing  whitespace    ";
    String withTabulator = "this\tstring \t\t  has\t tabs";
    String withNewline = "this\nstring \n\n  has\n newlines";

    List<String> strList = List.of(allWhitespace, withTrailingSpace, withTabulator, withNewline);

    for (String strToNormalize : strList) {
      String result = builtinSolution(strToNormalize);
      System.out.println("builtin    " + (result.length()) + ": " + result);

      result = stringBuilderSolution(strToNormalize);
      System.out.println("strBuilder " + (result.length()) + ": " + result);

      char[] normalized = slidingWindowInPlaceSolution(strToNormalize.toCharArray());
      result = String.valueOf(normalized);
      System.out.println("sliding    " + (result.length()) + ": " + result);

      System.out.println("--");

    }
  }

  static String builtinSolution(String strToNormalize) {
    return strToNormalize
      .replaceAll("\\s+", " ")
      .trim();
  }

  static String stringBuilderSolution(String strToNormalize) {
    StringBuilder builder = new StringBuilder();

    boolean isFirstWord = true;
    boolean inWhitespace = false;

    for (char character : strToNormalize.toCharArray()) {
      if (Character.isWhitespace(character)) {
        inWhitespace = true;
      } else {

        if (inWhitespace && !isFirstWord) {
          builder.append(" ");
        }

        inWhitespace = false;
        isFirstWord = false;
        builder.append(character);
      }
    }

    return builder.toString();
  }

  //true inplace edit is impossible in java because arrays cannot be resized
  static char[] slidingWindowInPlaceSolution(char[] strToNormalize) {
    boolean isFirstWord = true;
    boolean inWhitespace = false;
    int i = 0;

    for (char c : strToNormalize) {
      if (Character.isWhitespace(c)) {
        inWhitespace = true;
      } else {
        if (inWhitespace && !isFirstWord) {
          strToNormalize[i] = ' ';
          i++;
        }

        inWhitespace = false;
        isFirstWord = false;

        strToNormalize[i] = (c);
        i++;
      }
    }

    return Arrays.copyOf(strToNormalize, i);
  }
}
