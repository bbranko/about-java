package kds.java.challanges.slidingWindow;

/*
  Given array and size of a window, return an array containing the maximum value from each segment.
 */

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

public class MaxInWindow {
  public static void main(String[] args) {
    int[] result = maxSlidingWindow(new int[]{1, 3, -1, -3, 5, 3, 6, 7}, 3);
    System.out.println(Arrays.toString(result));
    result = maxSlidingWindow(new int[]{1}, 1);
    System.out.println(Arrays.toString(result));
    //the worst case scenario, where max in range gets called after every iteration
    result = maxSlidingWindow(new int[]{6, 5, 4, 3, 2, 1}, 3);
    System.out.println(Arrays.toString(result));
    result = maxSlidingWindow(new int[]{1, 2, 3, 4, 5, 6}, 3);
    System.out.println(Arrays.toString(result));

    System.out.println("--");

    result = maxSlidingWindowWithQueue(new int[]{1, 3, -1, -3, 5, 3, 6, 7}, 3);
    System.out.println(Arrays.toString(result));
    result = maxSlidingWindowWithQueue(new int[]{1}, 1);
    System.out.println(Arrays.toString(result));
    result = maxSlidingWindowWithQueue(new int[]{6, 5, 4, 3, 2, 1}, 3);
    System.out.println(Arrays.toString(result));
    //worst case scenario, but comes at additional cost of popping one element and pushing another into a queue
    result = maxSlidingWindowWithQueue(new int[]{1, 2, 3, 4, 5, 6}, 3);
    System.out.println(Arrays.toString(result));

  }


  //worst case scenario and this becomes O(nk), but space is 0(1)
  //there is a better solution where we preserve the next potential maximums in queue,
  // thus ensuring that we only ever pass through the array once.
  static int[] maxSlidingWindow(int[] nums, int k) {
    int[] result = new int[nums.length - k + 1];

    int wStart = 0;
    int wEnd = k - 1;
    int wMaxAtIndex = -1;

    while (wEnd < nums.length) {
      if (wStart > wMaxAtIndex) {
        wMaxAtIndex = maxInRange(nums, wStart, wEnd);
      }

      if (nums[wMaxAtIndex] < nums[wEnd]) {
        System.out.println("check new element");
        wMaxAtIndex = wEnd;
      }

      System.out.println("write result");
      result[wStart] = nums[wMaxAtIndex];

      wStart++;
      wEnd++;
    }

    return result;
  }

  static int maxInRange(int[] nums, int start, int end) {
    int maxAtIndex = start;
    for (int i = start + 1; i <= end; i++) {
      System.out.println("looking for max in range...");
      if (nums[maxAtIndex] < nums[i]) {
        maxAtIndex = i;
      }
    }
    return maxAtIndex;
  }

  //better O(n) solution, but now space needed is O(k)
  static int[] maxSlidingWindowWithQueue(int[] nums, int k) {
    Deque<Integer> maxAtIndexQueue = new ArrayDeque<>();
    int[] result = new int[nums.length - k + 1];

    //hint: thinking about what needs to be done backwards helps
    for (int i = 0; i < nums.length; i++) {
      while (!maxAtIndexQueue.isEmpty() && nums[maxAtIndexQueue.getLast()] < nums[i]) {
        //Remove all numbers lower than current, starting from behind.
        // This will keep the queue always sorted and with relevant data only.
        System.out.println("clean junk");
        maxAtIndexQueue.removeLast();
      }
      //we add next maxAtIndex
      System.out.println("add candidate");
      maxAtIndexQueue.addLast(i);

      //k-1 since k is size and indexes are 0 based; consider k-1 as window offset.
      //if(maxAtIndexQueue.getFirst() < (i-(k-1))){
      if (maxAtIndexQueue.getFirst() == (i - k)) {
        //the current maxAtIndex is out of bounds at the lower end
        System.out.println("clean out of bounds");
        maxAtIndexQueue.removeFirst();
      }

      //start writing output only after a window has been passed
      if (i >= k - 1) {
        System.out.println("write result");
        result[i - (k - 1)] = nums[maxAtIndexQueue.getFirst()];
      }
    }

    return result;
  }

}
