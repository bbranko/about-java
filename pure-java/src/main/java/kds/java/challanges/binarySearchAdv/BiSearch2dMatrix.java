package kds.java.challanges.binarySearchAdv;

//Given an MxN int matrix, where:
// - Each row is sorted in non-decreasing order.
// - First integer of each row is greater than the last integer of the previous row.
//
//Given an integer target, return index, or null otherwise.
//Solution must have O(log(m * n)) time complexity.

//note: log(a) + log(b) == log(a*b) - QuickMaphs... >.>

import java.util.Arrays;
import java.util.function.Function;

public class BiSearch2dMatrix {

  public static void main(String[] args) {
    int[][] matrix = {
      {11, 12, 13, 14},
      {21, 22, 23, 24},
      {31, 32, 33, 34},
      {41, 42, 43, 44},
      {51, 52, 53, 54},
    };

    int[] foundAt = searchMatrix(matrix, 21);
    System.out.println("21 found at: " + Arrays.toString(foundAt));
    foundAt = searchMatrix(matrix, 99);
    System.out.println("99 found at: " + Arrays.toString(foundAt));
    System.out.println("--now with generic solution--");
    foundAt = searchMatrixGeneralized(matrix, 21);
    System.out.println("21 found at: " + Arrays.toString(foundAt));
    foundAt = searchMatrixGeneralized(matrix, 99);
    System.out.println("99 found at: " + Arrays.toString(foundAt));
  }

  //basically apply binary search twice in succession... algo CAN BE GENERALIZED though (see next functions...)
  public static int[] searchMatrix(int[][] matrix, int target) {
    int rowLow = 0;
    int rowHigh = matrix.length - 1;
    int rowIndex = -1;

    while (rowLow <= rowHigh) {
      rowIndex = (rowLow + rowHigh) / 2;
      if (target > matrix[rowIndex][0]) {
        rowLow = rowIndex + 1;
      } else if (target < matrix[rowIndex][0]) {
        rowHigh = rowIndex - 1;
      } else {
        //target found;
        break;
      }
    }

    //the matrix should be rectangular, but when written like this we just do not care
    int colLow = 0;
    int colHigh = matrix[rowIndex].length - 1;
    int colIndex = -1;
    while (colLow <= colHigh) {
      colIndex = (colLow + colHigh) / 2;
      if (target > matrix[rowIndex][colIndex]) {
        colLow = colIndex + 1;
      } else if (target < matrix[rowIndex][colIndex]) {
        colHigh = colIndex - 1;
      } else {
        //target found;
        break;
      }
    }

    return (matrix[rowIndex][colIndex] == target) ? new int[]{rowIndex, colIndex} : null;
  }


  //algo can be generalized for arbitrary dimension matrix,
  // by extracting BiSearch and providing conversion lambda for getting values...
  public static int[] searchMatrixGeneralized(final int[][] matrix, int target) {
    //note: matrix does not have to be 2d, code will just go on, and on, and on...
    final int rowIndex = getClosestIndex(
      (index) -> matrix[index][0],
      matrix.length,
      target
    );

    final int colIndex = getClosestIndex(
      (index) -> matrix[rowIndex][index],
      matrix[rowIndex].length,
      target
    );

    return (matrix[rowIndex][colIndex] == target) ? new int[]{rowIndex, colIndex} : null;
  }

  public static int getClosestIndex(Function<Integer, Integer> getter, int size, int target) {
    //binary search algorithm, but we are getting data from an arbitrary source
    int low = 0;
    int high = size - 1;
    int index = -1;
    while (low <= high) {
      index = (low + high) / 2;
      if (target > getter.apply(index)) {
        low = index + 1;
      } else if (target < getter.apply(index)) {
        high = index - 1;
      } else {
        //target found;
        break;
      }
    }

    return index;
  }
}
