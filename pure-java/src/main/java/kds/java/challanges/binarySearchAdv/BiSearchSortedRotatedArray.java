package kds.java.challanges.binarySearchAdv;

//Given a sorted int array find targets index.
// - Array may contain duplicates. (though we really do not care if there are duplicates or not...)
// - Array may be rotated. Ie instead [1,2,3,3,4] it may be [3,4,1,2,3]
//
//Provide the most efficient solution.

public class BiSearchSortedRotatedArray {

  public static void main(String[] args) {
    int foundAt;
    for (int i = 0; i < 6; i++) {
      foundAt = search(new int[]{1, 2, 3, 3, 4}, i);
      System.out.println(i + " found at: " + foundAt);
    }

    System.out.println("--rotated--");

    for (int i = 0; i < 6; i++) {
      foundAt = search(new int[]{3, 4, 1, 2, 3}, i);
      System.out.println(i + " found at: " + foundAt);
    }
  }

  static int search(int[] nums, int target) {
    //start same as regular binary search
    int low = 0;
    int high = nums.length - 1;
    int index;

    //...same end condition
    while (low <= high) {
      index = (low + high) / 2;

      //short-circuit because it is simpler...
      if (target == nums[index]) {
        //also, if there is a need to stabilize algo, this is the spot
        // loop here through to either a first or last duplicate (note that it has to be with wrap around)
        return index;
      } else if (nums[low] <= nums[index]) { //if the left part is sorted
        //...check if the target is in the left part
        if (target < nums[index] && target >= nums[low]) {
          high = index - 1;
        } else {
          //...otherwise move search to the right part.
          low = index + 1;
        }
      } else { //since we got here, the right part must be sorted
        //...check if target is in the right part
        if (target > nums[index] && target <= nums[high]) {
          low = index + 1;
        } else {
          //...otherwise move search to the left part
          high = index - 1;
        }
      }
    }

    return -1;
  }
}
