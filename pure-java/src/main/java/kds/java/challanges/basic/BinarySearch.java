package kds.java.challanges.basic;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BinarySearch {
  public static void main(String[] args) {
    List<Integer> sortedArray = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);

    int numberToFind = 3;

    int foundAt = Arrays.binarySearch(sortedArray.toArray(), numberToFind);
    System.out.println("Number " + numberToFind + " found at " + foundAt);
    System.out.println("Value check: " + sortedArray.get(foundAt));
    System.out.println("--");

    //foundAt = Collections.binarySearch(sortedArray, numberToFind);
    foundAt = Collections.binarySearch(sortedArray, numberToFind, Integer::compareTo); //we can also pass comparator
    System.out.println("Number " + numberToFind + " found at " + foundAt);
    System.out.println("Value check: " + sortedArray.get(foundAt));
    System.out.println("--");

    //myBinarySearch has print-lines within it ;)
    myBinarySearch(sortedArray, numberToFind);
    System.out.println("--");
    myBinarySearch(sortedArray, 11);
  }

  static int myBinarySearch(List<Integer> sortedArray, int numberToFind) {
    //binary search needs two additional indexes, and it cuts possible location of numberToFind by two with each iteration
    //making binary search have O(log(n)) time complexity
    int lowIndex = 0;
    int highIndex = sortedArray.size() - 1;

    while (lowIndex <= highIndex) {
      //indexToCheck is always in the middle of other indexes
      int indexToCheck = lowIndex + (highIndex - lowIndex) / 2;
      //int indexToCheck = (lowIndex + highIndex) / 2; //can overflow even if Java binarySearch algos count index in the same manner...
      System.out.println("low[" + lowIndex + "] checking[" + indexToCheck + "] high[" + highIndex + "]");

      int numberToCheck = sortedArray.get(indexToCheck);
      if (numberToFind > numberToCheck) {
        lowIndex = indexToCheck + 1;
      } else if (numberToFind < numberToCheck) {
        highIndex = indexToCheck - 1;
      } else {
        //in general, it is actually worse to put this at the top...
        System.out.println("Number found at: " + indexToCheck);
        return indexToCheck;
      }
    }

    System.out.println("Number NOT found!");
    return -1;
  }
}
