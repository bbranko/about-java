package kds.java.challanges.basic;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;

/**
 * The basic way to traverse a graph (or a tree) is DFS or BFS.
 * Acronyms are short for Depth/Breadth - First - Search.
 * Trees are acyclic, while graphs can have cycles, which is why we need to track visited nodes when traversing graphs!
 * <p>
 * DFS (Pre-order) goes as far down one side (usually left) as it cans before going to the next path.
 * Order: root - dfs(left) - dfs(right)
 * Implement with recursion or stack (LIFO order)
 * <p>
 * BFS (Level-order) goes level by level.
 * Order: root - bfs(left) - bfs(right)
 * Implement with queue (FIFO order)
 * <p>
 * Others to add: Post-order (DFS but print root last), In-order (DFS but left-root-right)
 */
public class TreeTraversal {

  public static void main(String[] args) {
    //init data
    IntBinaryTree node42 = new IntBinaryTree(42, null, null);
    IntBinaryTree node31 = new IntBinaryTree(31, null, node42);
    IntBinaryTree node32 = new IntBinaryTree(32, null, null);
    IntBinaryTree node34 = new IntBinaryTree(34, null, null);
    IntBinaryTree node21 = new IntBinaryTree(22, node31, node32);
    IntBinaryTree node22 = new IntBinaryTree(21, null, node34);
    IntBinaryTree treeRoot = new IntBinaryTree(1, node21, node22);

    /*
     data looks like:
               1
           21         22
       31     32    x    34
     x   42  x  x       x  x
     */

    System.out.println("DFS recursive: 1, 21, 31, 42, 32, 22, 34");
    DFSRecursive(treeRoot, System.out::println);
    System.out.println("DFS looped: 1, 21, 31, 42, 32, 22, 34");
    DFS(treeRoot, System.out::println);

    System.out.println("BFS looped: 1, 21, 22, 31, 32, 34, 42");
    BFS(treeRoot, System.out::println);
  }

  private static <T> void DFSRecursive(BiTree<T> treeRoot, Consumer<T> consumer) {
    if (treeRoot == null) {
      //reached end, return
      return;
    }

    consumer.accept(treeRoot.getData());
    DFSRecursive(treeRoot.getLeft(), consumer);
    DFSRecursive(treeRoot.getRight(), consumer);
  }

  private static <T> void DFS(BiTree<T> treeRoot, Consumer<T> consumer) {
    if (treeRoot == null) {
      //root node non-existent, nothing to show
      return;
    }

    Stack<BiTree<T>> stack = new Stack<>();
    stack.push(treeRoot);
    while (!stack.empty()) {
      BiTree<T> treeNode = stack.pop();
      consumer.accept(treeNode.getData());

      //note, due to usage of a stack, we need to push next elements in reverse order
      if (treeNode.getRight() != null) {
        stack.push(treeNode.getRight());
      }
      if (treeNode.getLeft() != null) {
        stack.push(treeNode.getLeft());
      }
    }
  }

  private static <T> void BFS(BiTree<T> treeRoot, Consumer<T> consumer) {
    if (treeRoot == null) {
      //root node non-existent, nothing to show
      return;
    }

    Deque<BiTree<T>> queue = new ArrayDeque<>();
    queue.addFirst(treeRoot);
    while (queue.size() > 0) {
      BiTree<T> treeNode = queue.removeLast();
      consumer.accept(treeNode.getData());

      if (treeNode.getLeft() != null) {
        queue.addFirst(treeNode.getLeft());
      }
      if (treeNode.getRight() != null) {
        queue.addFirst(treeNode.getRight());
      }
    }
  }

}

interface BiTree<T> {
  T getData();
  BiTree<T> getLeft();
  BiTree<T> getRight();
}

@Data
@AllArgsConstructor
class IntBinaryTree implements BiTree<Integer> {
  Integer data;
  IntBinaryTree left;
  IntBinaryTree right;
}
