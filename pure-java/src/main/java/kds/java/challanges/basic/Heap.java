package kds.java.challanges.basic;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Heap is a complete BinaryTree where a root element is always smaller (minHeap) or greater (maxHeap) than its leafs.
 * Being a complete tree, it is a convenient structure to store in an Array with a bit of index counting to move around.
 */
public class Heap {
  public static void main(String[] args) {
    //builtin generic heap structure is PriorityQueue, pass Comparator for custom ordering
    PriorityQueue<Integer> builtinHeap = new PriorityQueue<>();

    MinIntHeap minHeap = new MinIntHeap();
    for (int value : List.of(10, 17, 20, 15, 8, 12, 2)) {
      minHeap.push(value);
      builtinHeap.add(value);
      System.out.println(minHeap.items);
      System.out.println(builtinHeap);
    }
    System.out.println(minHeap);

    int amount = 2;
    while (amount-- > 0) {
      minHeap.pop();
      builtinHeap.poll();
      System.out.println(minHeap.items);
      System.out.println(builtinHeap);
    }
    System.out.println(minHeap);
  }
}

class MinIntHeap {
  List<Integer> items = new ArrayList<>();
  //Alternative is to have int[], size and a way to resize an array. Basically what ArrayList already does for us;

  //@formatter:off
  int getLeftChildIndex(int parentIndex) { return parentIndex * 2 + 1; }
  int getRightChildIndex(int parentIndex) { return parentIndex * 2 + 2; }
  int getParentIndex(int childIndex) {
    //int operation so it rounds down for us ;)
    return (childIndex - 1) / 2;
  }

  boolean hasLeftChild(int index) { return getLeftChildIndex(index) < items.size(); }
  boolean hasRightChild(int index) { return getRightChildIndex(index) < items.size(); }
  boolean hasParent(int index) { return getParentIndex(index) >= 0; }

  int getLeftChild(int index) { return items.get(getLeftChildIndex(index)); }
  int getRightChild(int index) { return items.get(getRightChildIndex(index)); }
  int getParent(int index) { return items.get(getParentIndex(index)); }
  //@formatter:on

  void swap(int index1, int index2) {
    //int temp = items.get(index1);
    //items.set(index1, items.get(index2));
    //items.set(index2, temp);

    //because .set returns a previous value, we can actually write swap as oneliner
    items.set(index1, items.set(index2, items.get(index1)));
  }

  //Correct heap starting from the ROOT element and moving down. Also named siftDown
  void heapifyDown() {
    int index = 0;
    while (hasLeftChild(index)) { //if node has children, it will always have left one first!
      int smallerChildIndex = getLeftChildIndex(index);
      if (hasRightChild(index) && getRightChild(index) < getLeftChild(index)) {
        smallerChildIndex = getRightChildIndex(index);
      }

      if (items.get(index) > items.get(smallerChildIndex)) {
        swap(index, smallerChildIndex);
        index = smallerChildIndex;
      } else {
        break;
      }
    }
  }

  //Correct heap starting from the LAST element and moving up. Also named siftUp
  void heapifyUp() {
    int index = items.size() - 1;
    while (hasParent(index) && getParent(index) > items.get(index)) {
      swap(getParentIndex(index), index);
      index = getParentIndex(index);
    }
  }

  //just check on a first element
  int peek() {
    return items.get(0); //throws exception if items are empty
  }

  //removes root, in case of minHeap that is always the lowest element
  int pop() {
    if (items.size() > 1) {
      swap(0, items.size() - 1);                      //first, we swap the first and last element.
    }
    int item = items.remove(items.size() - 1);  //note, throws exception if heap is empty!
    heapifyDown();                                    //we then correct the heap
    return item;
  }

  //pushes a new value into minHeap
  void push(int value) {
    items.add(value); //add as last
    heapifyUp();      //correct heap
  }

  //Because java f-ing does not have log2 function :/
  double log2(int value) {
    // calculate log2 of value indirectly, via log() method
    return Math.log(value) / Math.log(2);
  }

  int calcDepth(int index) {
    return (int) log2(index + 1);
  }

  @Override
  public String toString() {
    //using BFS, we can easily do a pretty print
    //because heap is a complete tree, we can calculate depth based on index
    int maxDepth = calcDepth(items.size());
    //hack: to correct for edge-case when the last level is fully full
    if (maxDepth > 0 && log2(items.size() + 1) - maxDepth == 0) {
      maxDepth--;
    }


    StringBuilder builder = new StringBuilder();
    //bfs through complete tree stored in an array is just iterating over the array :D
    for (int index = 0; index < items.size(); index++) {
      int currentDepth = calcDepth(index);
      int spaces = (int) Math.pow(maxDepth - currentDepth + 2, 2);

      for (int i = 0; i++ < spaces; ) {
        builder.append(" ");
      }
      builder.append(items.get(index));

      if (currentDepth < calcDepth(index + 1) && index < items.size()-1) {
        builder.append("\n");
      }
    }

    return builder.toString();
  }
}
