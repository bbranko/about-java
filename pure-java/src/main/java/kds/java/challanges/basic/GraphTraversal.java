package kds.java.challanges.basic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import java.util.*;
import java.util.function.Consumer;

/**
 * The basic way to traverse a graph (or a tree) is DFS or BFS.
 * Acronyms are short for Depth/Breadth - First - Search.
 * Trees are acyclic, while graphs can have cycles, which is why we need to track visited nodes when traversing graphs!
 * <p>
 * DFS (Pre-order) goes as far down one side (usually left) as it cans before going to the next path.
 * Order: root - dfs(left) - dfs(right)
 * Implement with recursion or stack (LIFO order)
 * <p>
 * BFS (Level-order) goes level by level.
 * Order: root - bfs(left) - bfs(right)
 * Implement with queue (FIFO order)
 * <p>
 * Others to add: Post-order (DFS but print root last), In-order (DFS but left-root-right)
 * --
 * todo Graph specific: find if graph has cycles, find the longest one, find how many there are
 */
public class GraphTraversal {

  public static void main(String[] args) {
    //init data
    IntGraph node51 = new IntGraph(51, null);
    IntGraph node41 = new IntGraph(41, List.of(node51));
    IntGraph node34 = new IntGraph(34, List.of());
    IntGraph node33 = new IntGraph(33, List.of());
    IntGraph node32 = new IntGraph(32, List.of());
    IntGraph node31 = new IntGraph(31, List.of(node41));
    node51.setConnections(List.of(node31));
    IntGraph node23 = new IntGraph(23, List.of(node32, node33, node34));
    IntGraph node22 = new IntGraph(22, null);
    IntGraph node21 = new IntGraph(21, List.of(node31, node22));
    node22.setConnections(List.of(node21));
    IntGraph graphRoot = new IntGraph(11, List.of(node21, node22, node23));

    /*
     directed graph from lowest to higher data looks like:
     11  --> 21  -->  31 --> 41
     |   \. ||        '\     |.
     |.    22           \<-- 51
     23 --> 32
     |. \.
     34  33

     */

    System.out.println("DFS looped: 11, 21, 31, 41, 51, (hitVisited), 22, (hitVisited), 23, 32, 33, 34");
    DFSgraph(graphRoot, System.out::println);
    System.out.println("DFS looped: 11, 21, 31, 41, 51, (hitVisited), 22, (hitVisited), 23, 32, 33, 34");
    DFSgraphSimplified(graphRoot, System.out::println);

    System.out.println("BFS looped: 11, 21, 22, 23, 31, (hitVisited), 32, 33, 34, 41, 51, (hitVisited)");
    BFSgraph(graphRoot, System.out::println);
    System.out.println("BFS looped: 11, 21, 22, 23, 31, (hitVisited), 32, 33, 34, 41, 51, (hitVisited)");
    BFSgraphSimplified(graphRoot, System.out::println);
  }


  private static <T> void DFSgraph(Graph<T> graphRoot, Consumer<T> consumer) {
    if (graphRoot == null) {
      //root node non-existent, nothing to show
      return;
    }

    //1 of 2 changes compared to navigating a tree
    Set<Graph<T>> visited = new HashSet<>();
    Stack<Graph<T>> stack = new Stack<>();
    stack.push(graphRoot);
    //even though it is a double loop, complexity is O(n) since we go through all elements once!
    while (!stack.empty()) {
      Graph<T> graphNode = stack.pop();
      consumer.accept(graphNode.getData());
      visited.add(graphNode);

      //note, due to usage of a stack, we need to push next elements in reverse order
      //this creates iterator and sets start point to last element
      //we could also iterate with for loop and i--
      ListIterator<? extends Graph<T>> iterator = graphNode.getConnections()
        .listIterator(graphNode.getConnections().size());
      //2 of 2 changes compared to navigating a tree
      while (iterator.hasPrevious()) {
        Graph<T> connectedNode = iterator.previous();
        if (visited.contains(connectedNode)) {
          System.out.println("going from " + graphNode.getData() + " hit visited " + connectedNode.getData());
        } else if (stack.contains(connectedNode)) {
          System.out.println("going from " + graphNode.getData() + " hit planned " + connectedNode.getData());
        } else {
          stack.push(connectedNode);
        }
      }

    }
  }

  private static <T> void DFSgraphSimplified(Graph<T> graphRoot, Consumer<T> consumer) {
    if (graphRoot == null) {
      //root node non-existent, nothing to show
      return;
    }

    Set<Graph<T>> visited = new HashSet<>();
    Stack<Graph<T>> stack = new Stack<>();
    stack.push(graphRoot);

    while (!stack.empty()) {
      Graph<T> graphNode = stack.pop();
      if(!visited.contains(graphNode)) {
        visited.add(graphNode);
        consumer.accept(graphNode.getData());

        //note, due to usage of a stack, we need to push next elements in reverse order
        ListIterator<? extends Graph<T>> iterator = graphNode.getConnections()
          .listIterator(graphNode.getConnections().size());
        while (iterator.hasPrevious()) {
          Graph<T> connectedNode = iterator.previous();
          stack.push(connectedNode);
        }
      } else {
        System.out.println("hit visited " + graphNode.getData());
      }
    }
  }

  private static <T> void BFSgraph(Graph<T> graphRoot, Consumer<T> consumer) {
    if (graphRoot == null) {
      //root node non-existent, nothing to show
      return;
    }

    //1 of 2 changes compared to navigating a tree
    Set<Graph<T>> visited = new HashSet<>();
    Deque<Graph<T>> queue = new ArrayDeque<>();
    queue.addFirst(graphRoot);
    //even though it is a double loop, complexity is O(n) since we go through all elements once!
    while (queue.size() > 0) {
      Graph<T> graphNode = queue.removeLast();
      consumer.accept(graphNode.getData());
      visited.add(graphNode);

      //2 of 2 changes compared to navigating a tree
      for (Graph<T> connectedNode : graphNode.getConnections()) {
        if (visited.contains(connectedNode)) {
          System.out.println("going from " + graphNode.getData() + " hit visited " + connectedNode.getData());
        }  else if (queue.contains(connectedNode)) {
          System.out.println("going from " + graphNode.getData() + " hit planned " + connectedNode.getData());
        } else {
          queue.addFirst(connectedNode);
        }
      }
    }
  }

  private static <T> void BFSgraphSimplified(Graph<T> graphRoot, Consumer<T> consumer) {
    if (graphRoot == null) {
      //root node non-existent, nothing to show
      return;
    }

    Set<Graph<T>> visited = new HashSet<>();
    Deque<Graph<T>> queue = new ArrayDeque<>();
    queue.addFirst(graphRoot);

    while (queue.size() > 0) {
      Graph<T> graphNode = queue.removeLast();
      if (!visited.contains(graphNode)) {
        visited.add(graphNode);
        consumer.accept(graphNode.getData());

        for (Graph<T> connectedNode : graphNode.getConnections()) {
          queue.addFirst(connectedNode);
        }
      } else {
        System.out.println("hit visited " + graphNode.getData());
      }
    }
  }

}


interface Graph<T> {
  T getData();
  List<? extends Graph<T>> getConnections();
}

@Data
@AllArgsConstructor
class IntGraph implements Graph<Integer> {
  Integer data;
  //otherwise, lombok will create infinite loop when calculating hash for HashSet!
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  List<IntGraph> connections;
}
