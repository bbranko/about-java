package kds.java.tasks.dataStructuresTaskReview;

public interface IntCollection {
  //Primitive type as an input to prevent possibility of passing a null
  //Boxed primitive type as an output to allow returning of null
  void add(int value);
  void remove(int value);
  Boolean contains(int value);
}
