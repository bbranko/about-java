package kds.java.tasks.dataStructuresTaskReview;

public interface IntList extends IntCollection {
  Integer get(int index);
  void addAtIndex(int index, int value);
}
